package Domain;

import java.util.ArrayList;

public class ListPlayerAnswer {
    private ArrayList listPlayerAnswer = new ArrayList();

    public ListPlayerAnswer(ArrayList listanswer) {
        this.listPlayerAnswer = listPlayerAnswer;
    }

    public ListPlayerAnswer() {

    }

    public ArrayList getListPlayerAnswer() {
        return listPlayerAnswer;
    }

    public void add(String answer) {
        listPlayerAnswer.add(answer);
    }
}
