package Domain;

import java.util.ArrayList;

public class ListAnswer {
    private ArrayList listAnswer = new ArrayList();

    public ListAnswer(ArrayList listAnswer) {
        this.listAnswer = listAnswer;
    }

    public ListAnswer() {

    }

    public ArrayList getListAnswer() {
        return listAnswer;
    }
    public void add(String answer) {
        listAnswer.add(answer);
    }

}
