package Domain;

import java.util.List;

public class Question {
    private int numberOfQuestion;
    private String nameOfQuestion;
    private String answers;
    private String trueanswers;

    public Question(int numberOfQuestion, String nameOfQuestion, String answers, String trueanswers) {
        if (numberOfQuestion<=0){
            throw new IllegalArgumentException();
        }
        this.numberOfQuestion = numberOfQuestion;
        this.nameOfQuestion = nameOfQuestion;
        this.answers = answers;
        this.trueanswers = trueanswers;
    }

    public int getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public String getNameOfQuestion() {
        return nameOfQuestion;
    }

    public String getAnswers() {
        return answers;
    }

    public String getTrueanswers() {
        return trueanswers;
    }
}
