package Cli;

import Cli.Screen.CollectionScreen;
import Cli.Screen.InputManager;
import Cli.Screen.MainScreen;
import Cli.Screen.PooScreen;
import java.util.Scanner;

public class Application {
    static void application (){
        Scanner scanner = new Scanner(System.in);
        MainScreen mainscreen = new MainScreen();
        PooScreen pooscreen = new PooScreen();
        CollectionScreen collectionscreen = new CollectionScreen();

        int playerchoice;

        try {
            do {
                mainscreen.display(scanner);
                playerchoice = InputManager.playerChoice();

                if (playerchoice ==1) {
                    pooscreen.display(scanner);
                }
                else if (playerchoice==2) {
                    collectionscreen.display(scanner);

                }
                else if (playerchoice==3){
                    System.out.print("Goodbye !");

                }
                else{
                    System.out.println("Entrer un nombre valide");
                }


            }
            while (playerchoice!=3);
        } catch (NumberFormatException e){
            System.out.println("Entrer un nombre valide");
        }





    }
}
