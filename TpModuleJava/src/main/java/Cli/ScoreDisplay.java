package Cli;

import Cli.Screen.InputManager;

import java.util.ArrayList;

public class ScoreDisplay {
    public static void score(int score, ArrayList<Integer> falseAnswer, ArrayList<String> goodAnswer) {
        String playerAnswer;
        if (score == 10) {
            System.out.println("= Résultat du QCM\n" +
                    "\n" +
                    "Bravo ! Vous avez fait un sans faute et obtenu un 10/10.\n");
        } else {
            do {
                System.out.print("!! Résultat du QCM !!\n" +
                        "\n" +
                        "Votre note est de " + score + "/10.\n" +
                        "\n" +
                        "Voulez-vous voir les réponses des questions où vous avez échoué (y/n) ? \n");

                playerAnswer = InputManager.playerAnswer();
                if (playerAnswer.equalsIgnoreCase("y")) {
                    System.out.println("\nVoici les réponses : ");
                    for (Integer i : falseAnswer) {
                        System.out.println("\nQuestion " + (i + 1));
                        System.out.println(goodAnswer.get(i)+"\n");

                    }

                } else if (playerAnswer.equalsIgnoreCase("n")) {
                    System.out.println("Ok ! Bye !");

                }
            }
            while (!playerAnswer.equalsIgnoreCase("y") && !playerAnswer.equalsIgnoreCase("n"));


        }
    }
}
