package Cli.Screen;

import Cli.ScoreDisplay;
import Domain.ListAnswer;
import Domain.ListPlayerAnswer;

import java.util.ArrayList;
import java.util.Scanner;

public class CollectionScreen implements Screen {
    @Override
    public void display(Scanner scanner) {
        ListPlayerAnswer listPlayerAnswer = new ListPlayerAnswer();
        ListAnswer listAnswer = new ListAnswer();

        listAnswer.add("c");
        listAnswer.add("d");
        listAnswer.add("b");
        listAnswer.add("d");
        listAnswer.add("a");
        listAnswer.add("c");
        listAnswer.add("a");
        listAnswer.add("c");
        listAnswer.add("c");
        listAnswer.add("c");


        System.out.println("=== QCM Java - Les collections - Partie 1\n" +
                "\n" +
                "= Question 1\n" +
                "Lequel de ces packages contient toutes les classes de collection?\n" +
                "\n" +
                "    A - java.awt\n" +
                "    B - java.net\n" +
                "    C - java.util\n" +
                "    D - java.lang\n" +
                "\n" +
                "Votre réponse : \n");

        String playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);


        System.out.println(
                "= Question 2\n" +
                        "Laquelle de ces classes ne fait pas partie du framework collection en Java?\n" +
                        "\n" +
                        " A Queue\n" +
                        "\n" +
                        "B Stack\n" +
                        "\n" +
                        "C Array\n" +
                        "\n" +
                        "D Map" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);


        System.out.println(
                "= Question 3\n" +
                        "Laquelle de ces interfaces ne fait pas partie du framework collection en Java?\n" +
                        "\n" +
                        "A SortedMap\n" +
                        "\n" +
                        "B SortedList\n" +
                        "\n" +
                        "C Set\n" +
                        "\n" +
                        "D List" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 4\n" +
                        "Laquelle de ces méthodes supprime tous les éléments d’une collection?\n" +
                        "\n" +
                        " A refresh()\n" +
                        "\n" +
                        "B delete()\n" +
                        "\n" +
                        "C reset()\n" +
                        "\n" +
                        "D clear()" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 5\n" +
                        "Qu’est-ce que Collection en Java?\n" +

                        "A Un groupe d’objets\n" +
                        "\n" +
                        "B Un groupe d’interfaces\n" +
                        "\n" +
                        "C Un groupe de classes\n" +
                        "\n" +
                        "D Aucune de ces réponses n’est vraie." + "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 6\n" +
                        " Laquelle de ces interfaces ne fait pas partie du framework collection en Java?\n" +
                        "\n" +
                        " A Collection\n" +
                        "\n" +
                        "B Set\n" +
                        "\n" +
                        "C Group\n" +
                        "\n" +
                        "D List" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 7\n" +
                        "Quelle interface n’autorise pas les doublons?\n" +
                        "\n" +
                        " A Set\n" +
                        "\n" +
                        "B List\n" +
                        "\n" +
                        "C Map\n" +
                        "\n" +
                        "D Tout les réponses sont vrais" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 8\n" +
                        "Laquelle de ces classes de collection a la capacité d’évoluer de façon dynamique?\n" +
                        "\n" +
                        "A Array\n" +
                        "\n" +
                        "B Arrays\n" +
                        "\n" +
                        "C ArrayList\n" +
                        "\n" +
                        "D Tout les réponses sont vrais" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 9\n" +
                        "Un HashMap autorise _____________\n" +
                        "\n" +
                        "A des valeurs nulles\n" +
                        "\n" +
                        "B une clé nulle\n" +
                        "\n" +
                        "C Tout les réponses sont vrais\n" +
                        "\n" +
                        "D Aucune de ces réponses n’est vraie." +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 10\n" +
                        "L’efficacité d’un HashMap peut être garantie avec __________\n" +
                        "\n" +
                        " A La redéfinition de la méthode equals\n" +
                        "\n" +
                        "B La redéfinition de la méthode hashCode\n" +
                        "\n" +
                        "C Tout les réponses sont vrais\n" +
                        "\n" +
                        "D Aucune de ces réponses n’est vraie." +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);


        int score = 0;
        ArrayList<Integer> falseAnswer = new ArrayList<>();


        for (int i=0; i<10; i++){
            if (listPlayerAnswer.getListPlayerAnswer().get(i).equals(listAnswer.getListAnswer().get(i))){
                score+=1;
            }else falseAnswer.add(i);

        }
        ArrayList<String> goodAnswer = new ArrayList<>();
        goodAnswer.add("Les classes du Framework Collections se trouvent dans le package java.util.");
        goodAnswer.add("La classe Map ne fait pas partie du framework collection.");
        goodAnswer.add("L’interface SortedList ne fait pas partie du framework collection.");
        goodAnswer.add("La méthode clear() supprime tous les éléments d’une collection.");
        goodAnswer.add("Une collection est un objet qui représente un groupe d’objets.");
        goodAnswer.add("Group ne fait pas partie du framework collection.");
        goodAnswer.add("Voir : Différence entre List, Set et Map en java");
        goodAnswer.add("ArrayList est un tableau redimensionnable qui implémente l’interface List.");
        goodAnswer.add("HashMap autorise une clé nulle et les valeurs nulles (une seule clé nulle est autorisée car deux clés ne sont pas autorisées). Par contre Hashtable n’autorise pas les clés nulles ou les valeurs nulles.");
        goodAnswer.add("HashMap s’appuie sur la méthode equals() et hashCode() pour comparer les clés et les valeurs.");

        ScoreDisplay.score(score, falseAnswer, goodAnswer);

    }
}

