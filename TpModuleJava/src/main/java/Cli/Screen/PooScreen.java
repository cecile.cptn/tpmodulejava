package Cli.Screen;

import Cli.ScoreDisplay;
import Domain.ListAnswer;
import Domain.ListPlayerAnswer;
import Domain.Question;

import java.util.ArrayList;
import java.util.Scanner;

public class PooScreen implements Screen {

    @Override
    public void display(Scanner scanner) {
        ListPlayerAnswer listPlayerAnswer = new ListPlayerAnswer();
        ListAnswer listAnswer = new ListAnswer();

        listAnswer.add("d");
        listAnswer.add("b");
        listAnswer.add("b");
        listAnswer.add("c");
        listAnswer.add("b");
        listAnswer.add("d");
        listAnswer.add("b");
        listAnswer.add("a");
        listAnswer.add("a");
        listAnswer.add("bde");



        System.out.println("=== QCM Java - Programmation orientée objet\n" +
                "\n" +
                "= Question 1\n" +
                "Lequel des éléments suivants n’est pas un concept POO en Java?\n" +
                "\n" +
                "    A - Héritage\n" +
                "    B - Encapsulation\n" +
                "    C - Polymorphisme\n" +
                "    D - Compilation\n" +
                "\n" +
                "Votre réponse : \n");

        String playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);


        System.out.println(
                "= Question 2\n" +
                        "Quand la surcharge de méthode est-elle déterminée?\n" +
                        "\n" +
                        "    A - Au moment de l’exécution\n" +
                        "    B - Au moment de la compilation\n" +
                        "    C - Au moment du codage\n" +
                        "    D - Au moment de l’exécution\n" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);


        System.out.println(
                "= Question 3\n" +
                        "Quand la surcharge ne se produit pas?\n" +
                        "\n" +
                        "    A - Quand il y a plusieurs méthodes avec le même nom mais avec une signature de méthode différente et un nombre ou un type de paramètres différent\n" +
                        "    B -  Quand il y a plusieurs méthodes avec le même nom, le même nombre de paramètres et le type mais une signature différente\n" +
                        "    C - Quand il y a plusieurs méthodes avec le même nom, la même signature, le même nombre de paramètres mais un type différent\n" +
                        "    D - Quand il y a plusieurs méthodes avec le même nom, la même signature mais avec différente signature\n" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 4\n" +
                        "Quel concept de Java est un moyen de convertir des objets du monde réel en termes de classe?\n" +
                        "\n" +
                        "    A - Polymorphisme\n" +
                        "    B - Encapsulation \n" +
                        "    C - Abstraction\n" +
                        "    D - Héritage\n" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 5\n" +
                        "Quel concept de Java est utilisé en combinant des méthodes et des attributs dans une classe?\n" +

                        "A - Polymorphisme\n" +
                        "\n" +
                        "B - Encapsulation\n" +
                        "\n" +
                        "C - Abstraction\n" +
                        "\n" +
                        "D - Héritage\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 6\n" +
                        "Comment ça s’appelle si un objet a son propre cycle de vie?\n" +
                        "\n" +
                        " A Agrégation\n" +
                        "\n" +
                        "B Composition\n" +
                        "\n" +
                        "C Encapsulation\n" +
                        "\n" +
                        "D Association" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 7\n" +
                        "Comment s’appelle-t-on dans le cas où l’objet d’une classe mère est détruit donc l’objet d’une classe fille sera détruit également?\n" +
                        "\n" +
                        " A Agrégation\n" +
                        "\n" +
                        "B Composition\n" +
                        "\n" +
                        "C Encapsulation\n" +
                        "\n" +
                        "D Association" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 8\n" +
                        "Comment s’appelle-t-on l’objet a son propre cycle de vie et l’objet d’une classe fille ne dépend pas à un autre objet d’une classe mère?\n" +
                        "\n" +
                        "A Agrégation\n" +
                        "\n" +
                        "B Composition\n" +
                        "\n" +
                        "C Encapsulation\n" +
                        "\n" +
                        "D Association" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 9\n" +
                        "La surcharge d’une méthode peut remplacer l’héritage et le polymorphisme?\n" +
                        "\n" +
                        " A Vrai\n" +
                        "\n" +
                        "B Faux" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);

        System.out.println(
                "= Question 10\n" +
                        "Quels keywords sont utilisés pour spécifier la visibilité des propriétés et des méthodes ?\n" +
                        "\n" +
                        " A final\n" +
                        "\n" +
                        "B private\n" +
                        "\n" +
                        "C abstract\n" +
                        "\n" +
                        "D protected\n" +
                        "\n" +
                        "E public" +
                        "\n" +
                        "Votre réponse : \n");

        playerAnswer = InputManager.playerAnswer();
        listPlayerAnswer.add(playerAnswer);


        int score = 0;
        ArrayList<Integer> falseAnswer = new ArrayList<Integer>();


        for (int i = 0; i < 10; i++) {
            if (listPlayerAnswer.getListPlayerAnswer().get(i).equals(listAnswer.getListAnswer().get(i))) {
                score += 1;
            } else falseAnswer.add(i);


        }

        ArrayList<String> goodAnswer = new ArrayList<>();
        goodAnswer.add("Il existe 4 concepts POO en Java. Héritage, encapsulation, polymorphisme et abstraction.");
        goodAnswer.add("La surcharge est déterminée au moment de la compilation.");
        goodAnswer.add("La surcharge survient lorsque plusieurs méthodes ont le même nom mais un constructeur différent et aussi quand la même signature mais un nombre différent de paramètres et / ou de type de paramètre.");
        goodAnswer.add("L’abstraction est un concept de définition des objets du monde réel en termes de classes ou d’interfaces.");
        goodAnswer.add("L’encapsulation est implémentée en combinant des méthodes et des attributs dans une classe. La classe agit comme un conteneur de propriétés d’encapsulation.");
        goodAnswer.add("C’est une relation où tous les objets ont leur propre cycle de vie. Cela se produit lorsque de nombreuses relations sont disponibles, au lieu de One To One ou One To Many.");
        goodAnswer.add("La composition se produit lorsque l’objet d’une classe fille est détruit si l’objet de la classe mère est détruit. La composition est également appelée une agrégation forte.");
        goodAnswer.add("L’agrégation se produit lorsque les objets ont leur propre cycle de vie et que l’objet d’une classe fille peut être associé à un seul objet d’une classe mère.");
        goodAnswer.add("L’agrégation se produit lorsque les objets ont leur propre cycle de vie et que l’objet d’une classe fille peut être associé à un seul objet d’une classe mère.");
        goodAnswer.add("les mots clés utilisés pour spécifier la visibilité des propriétés et des méthodes sont : private, protected, public.");


        ScoreDisplay.score(score, falseAnswer, goodAnswer);
    }

}
